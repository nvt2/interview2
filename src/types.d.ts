export type Therapy = {
	cell: string;
	name: string;
	cost: number;
};

export type Cell = {
	type: string;
	age: number;
	weight: number;
};
