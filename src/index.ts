import { getCells, getTherapies } from "./api";
import { Cell } from "./types";

type CompleteTherapy = {
	cell: string;
	cells: Cell[];
	name: string;
	cost: number;
};

export const sortAndMerge = (): Promise<CompleteTherapy[]> => {
	getCells().then((cells) => console.log(cells));
	/*
		Example:
		[
			{type: 'a23e0', age: 9, weight: 7},
		 	{type: 'a24e0', age: 6, weight: 8},
			{type: 'a23e0', age: 4, weight: 12}
		]
	*/
	getTherapies().then((therapies) => console.log(therapies));
	/*
		Example:
		[
			{cell: 'a23', name: 'flawless', cost: 324},
			{cell: 'a24', name: 'marvelous', cost: 361}
		]
	*/

	/* (map adding cells array + sort by sum of age and weight for all cells on that kind of therapy)
		return Example:
		[
			{
				cells: [{type: 'a24e0', age: 6, weight: 8}]
				cell: 'a24',
				name: 'marvelous',
				cost: 361
			},
			{
				cells: [{type: 'a23e0', age: 9, weight: 7}, {type: 'a23e0', age: 4, weight: 12}]
				cell: 'a23',
				name: 'flawless',
				cost: 324
			},
		]
	*/
	return null;
};
