export const getCells = async (): Promise<Cell[]> => {
	// to ask things related with what the candidate is developing
	// custom sort a complex array
	const cells: Cell[] = [];
	for (let index = 0; index < 1000; index++) {
		cells.push({
			type: "a" + (index % 100) + "e0",
			age: (index * index) % 11,
			weight: (index + index) % 13,
		});
	}

	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(cells);
		}, 2000);
	});
};

export const getTherapies = async (): Promise<Therapy[]> => {
	// to ask things related with what the candidate is developing
	// custom sort a complex array
	const therapies: any[] = [];
	const therapyNames = ["marvelous", "reallygood", "flawless"];
	for (let index = 0; index < 100; index++) {
		therapies.push({
			cell: "a" + index,
			name: therapyNames[index % 3],
			cost: index * index - index * 10 + 26,
		});
	}
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(therapies);
		}, 2000);
	});
};

// Cell: { type: 'a4e0', age: 6, weight: 8 }, { type: 'a4e0', age: 1, weight: 2 }
// Therapy   { cell: 'a4', name: 'flawless', cost: 129 },
// Output: { cell:'a4' cells: [{ type: 'a4e0', age: 6, weight: 8 }, { type: 'a4e0', age: 1, weight: 2 }], name: 'flawless', cost: 129 },

// Cell: {  type: 'a4e0', age: 6, weight: 8 }, { type: 'a4e0', age: 1, weight: 2 }, { type: 'a5e0', age: 3, weight: 3 }, { type: 'a5e0', age: 1, weight: 1 }
// Therapy   { cell: 'a4', name: 'flawless', cost: 129 }, { cell: 'a5', name: 'flawless', cost: 129 },
/* Output: 
{ cell: 'a5', cells: [{ type: 'a5e0', age: 3, weight: 3 }, { type: 'a5e0', age: 1, weight: 1 }], name: 'flawless', cost: 129 }
{ cell: 'a4', cells: [{ type: 'a4e0', age: 6, weight: 8 }, { type: 'a4e0', age: 1, weight: 2 }], name: 'flawless', cost: 129 }
*/
