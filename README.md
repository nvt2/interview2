There are 2 functions: getCells and getTherapies, both are promises that will retrieve an array of information.

Each cell have an identifier which is "type", and each therapy is for a specific type of cell.

However, the endpoints are not properly syncronized and cells have a suffix that therapies don't (eg: a1 -> a1e0).

We want to inject in each therapy an array of the cells that match the type specified in Therapy's "cell" property.

Finally, we want to sort therapies by the sum of weight and age of all of their cells.
